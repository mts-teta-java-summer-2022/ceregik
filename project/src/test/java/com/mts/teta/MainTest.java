package com.mts.teta;

import static com.mts.teta.PositiveNegativeDate.NegativeEnrich;
import static com.mts.teta.PositiveNegativeDate.PositiveEnrich;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.mts.teta.dto.Message;
import jdk.jfr.Description;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MainTest {

    @Test
    void sanityTest() {
        assertTrue(true);
    }

    @Test
    @DisplayName("Обогащение сообщения")
    @Description("Обогащение сообщений 200 сообщений 100 из которых негативные")
    public void testSuccessEnrichment() throws InterruptedException {
        int numberOfThreads = 100;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        EnrichmentService enrichmentService = new EnrichmentService();
        Message enrichmentTestMessage = new Message("{\"action\": \"button_click\",\"page\": \"book_card\",\"msisdn\": \"88005553535\"}", Message.EnrichmentType.MSISDN);
        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                Message errorEnrichmentTestMessage = new Message("{\"action\": \"button_click\",\"page\": \"book_card\",\"enrichment\": {\"firstName\": \"Vitya\",\"lastName\": \"PUPKIN\"}}", Message.EnrichmentType.MSISDN);
                enrichmentService.enrich(errorEnrichmentTestMessage);
                enrichmentService.enrich(enrichmentTestMessage);
                latch.countDown();
            });
        }
        latch.await();

        Assertions.assertAll(
                () -> assertEquals(numberOfThreads, PositiveEnrich.size()),
                () -> assertEquals(numberOfThreads, NegativeEnrich.size())
        );

        PositiveEnrich.forEach((context) -> {
            try {
                JSONAssert.assertEquals("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"88005553535\",\"enrichment\":{\"lastName\":\"Ivanov\",\"firstName\":\"Vasya\"}}", context, true);
            } catch (JSONException e) {
                assertEquals(0, 1);
            }
        });

        NegativeEnrich.forEach((context) -> { //Ошибочные обработки
            try {
                JSONAssert.assertEquals("{\"action\": \"button_click\",\"page\": \"book_card\",\"enrichment\": {\"firstName\": \"Vitya\",\"lastName\": \"PUPKIN\"}}", context, true);
            } catch (JSONException e) {
                assertEquals(0, 1);
            }
        });
    }
}
