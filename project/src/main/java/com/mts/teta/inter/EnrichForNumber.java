package com.mts.teta.inter;

import com.mts.teta.dto.Message;

public interface EnrichForNumber {
        public String enrichMessage(Message message);
}
