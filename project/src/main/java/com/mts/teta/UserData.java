package com.mts.teta;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserData {

    static Map<String, Map<String,String>> userData = new ConcurrentHashMap<>();

    static {
        userData.put("88005553535",Map.of("firstName","Vasya","lastName","Ivanov"));
        userData.put("88005553534",Map.of("firstName","Sergey","lastName","Belyy"));
        userData.put("88005553533",Map.of("firstName","Max","lastName","Lermontov"));
    }

}
