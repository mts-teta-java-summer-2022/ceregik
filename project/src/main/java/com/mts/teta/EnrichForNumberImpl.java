package com.mts.teta;

import com.mts.teta.dto.Message;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mts.teta.PositiveNegativeDate.NegativeEnrich;
import static com.mts.teta.PositiveNegativeDate.PositiveEnrich;

public class EnrichForNumberImpl implements com.mts.teta.inter.EnrichForNumber {

    @Override
    public String enrichMessage(Message message) {
        String msisdn;
        JSONObject jo;
        synchronized (message.getEnrichmentType()) {
            try {
                jo = new JSONObject(message.getContent());
                msisdn = (String) jo.get("msisdn");
                UserData.userData.get(msisdn);
            } catch (JSONException | ClassCastException exception) {
                NegativeEnrich.add(message.getContent());
                return message.getContent();
            }
            jo.put("enrichment",UserData.userData.get(msisdn));
            PositiveEnrich.add(jo.toString());
        }

        return jo.toString();
    }
}
