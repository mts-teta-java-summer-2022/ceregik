package com.mts.teta;
import com.mts.teta.dto.Message;

import static com.mts.teta.PositiveNegativeDate.NegativeEnrich;

public class EnrichmentService {

    public String enrich(Message message) {

        switch (message.getEnrichmentType()){
            case MSISDN: {
                EnrichForNumberImpl enrichForNumber = new EnrichForNumberImpl();
                return enrichForNumber.enrichMessage(message);
            }
        }
        Thread addNegativeEnrichThread = new Thread(() -> NegativeEnrich.add(message.getContent()));
        addNegativeEnrichThread.start();
        return message.getContent();
    }
}
